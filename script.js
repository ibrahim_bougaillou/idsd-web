function Etudiant(nom,note1,note2,note3){
    this.nom=nom;
    this.note1=note1;
    this.note2=note2;
    this.note3=note3;
    this.moy=(note1+note2+note3)/3;
    this.affichInfo=function (){
        console.log(`Nom: ${this.nom}
        Note1; ${this.note1}
        Note2: ${this.note2}
        Note3: ${this.note3}
        Moy: ${this.moy}`);
    }
}
let e1= new Etudiant("reda",12,15,17)
let e2= new Etudiant("oussama",18,20,17.5)
e1.affichInfo()
e2.affichInfo()